#! /usr/bin/env python
import subprocess
import json
import time
import socket
BUFFER_SIZE = 1024

class AltElement(object):
    def __init__(self, alt_unity_driver, appium_driver, json_data):
        self.alt_unity_driver = alt_unity_driver
        self.appium_driver = appium_driver
        data = json.loads(json_data)
        self.name = str(data['name'])
        self.id = str(data['id'])
        self.x = str(data['x'] / alt_unity_driver.ratio['width'])
        self.y = str(data['y'] / alt_unity_driver.ratio['height'])
        self.mobileY = str(data['mobileY'] / alt_unity_driver.ratio['height'])
        self.text = (data['text']).encode('ascii', 'ignore').decode('ascii')
        self.type = str(data['type'])
        self.enabled = str(data['enabled'])
        self.width = int(data["width"])
        self.height = int(data["height"])
        self.parent = str(data['parent'])

    def update_coordinates(self, x, y, mobileY):
        self.x = str(float(x) / self.alt_unity_driver.ratio['width'])
        self.y = str(float(y) / self.alt_unity_driver.ratio['height'])
        self.mobileY = str(float(mobileY) / self.alt_unity_driver.ratio['height'])

    def toJSON(self):
        return '{"name":"' + self.name + '", \
                 "text":"' + self.text + '", \
                 "id":"' + self.id + '", \
                 "x":"' + self.x + '", \
                 "y":"' + self.y + '", \
                 "mobileY":"' + self.mobileY + '", \
                 "width":"' + str(self.width) + '", \
                 "height":"' + str(self.height) + '", \
                 "type":"' + self.type + '", \
                 "enabled":"' + self.enabled + '", \
                 "parent":"' + self.parent + '"}'

    def get_component_property(self, component_name, property_name):
        alt_object = self.toJSON()
        property_info = '{"component":"' + component_name + '", "property":"' + property_name + '"}'
        self.alt_unity_driver.socket.send('getObjectComponentProperty;' + alt_object + ';'+ property_info + ';&')
        data = self.alt_unity_driver.recvall()
        return data

    def set_component_property(self, component_name, property_name, value):
        alt_object = self.toJSON()
        property_info = '{"component":"' + component_name + '", "property":"' + property_name + '"}'
        self.alt_unity_driver.socket.send('setObjectComponentProperty;' + alt_object + ';'+ property_info + ';' + value + ';&')
        data = self.alt_unity_driver.recvall()
        return data

    def call_component_method(self, component_name, method_name, parameters):
        alt_object = self.toJSON()
        action_info = '{"component":"' + component_name + '", "method":"' + method_name + '", "parameters":"' + parameters + '"}'
        self.alt_unity_driver.socket.send('callComponentMethodForObject;' + alt_object + ';'+ action_info + ';&')
        data = self.alt_unity_driver.recvall()
        return data

    def get_text(self):
        alt_object = self.toJSON()
        self.alt_unity_driver.socket.send('getText;' + alt_object + ';&')
        data = self.alt_unity_driver.recvall()
        return data

    def tap(self, durationInSeconds=0.5):
        durationInMiliseconds = durationInSeconds * 1000
        if float(self.mobileY) <= self.alt_unity_driver.appium_resolution["height"] and float(self.x) <= self.alt_unity_driver.appium_resolution["width"]:
            # normal tap
            self.appium_driver.tap([[float(self.x), float(self.mobileY)]], durationInMiliseconds)
        else:
            # coords are out of the appium screen, try using shell to tap
            args = {
                "command": "input swipe",
                "args": [str(self.x), str(self.mobileY), str(self.x), str(self.mobileY), durationInMiliseconds]
            }
            self.appium_driver.execute_script("mobile: shell", args)

    def isOnScreen(self):
        return float(self.x) >= 0 and float(self.mobileY) >= 0 and float(self.x) <= self.alt_unity_driver.real_screen_size["width"] and float(self.mobileY) <= elf.alt_unity_driver.real_screen_size["height"]

    def dragTo(self, end_x, end_y, durationIndSeconds=0.5):
        self.appium_driver.swipe(self.x, self.mobileY, end_x, end_y, durationIndSeconds * 1000)

    def dragToElement(self, other_element, durationIndSeconds=0.5):
        self.appium_driver.swipe(self.x, self.mobileY, other_element.x, other_element.mobileY, durationIndSeconds * 1000)
    
    def update_element(self):
        updated = self.alt_unity_driver.find_element(self.name)
        self.update_coordinates(updated.x, updated.y, updated.mobileY)
        print("Updated elements!")
                

class AltrunUnityDriver(object):

    def __init__(self, appium_driver,  platform, TCP_IP='127.0.0.1', TCP_FWD_PORT=13001, TCP_PORT=13000, timeout=60):
        self.TCP_PORT = TCP_PORT
        self.appium_driver = appium_driver
        self.ratio = {}
        print 'Staring tests on ' + platform
        while (timeout > 0):
            try:
                self.setup_port_forwarding(platform, TCP_FWD_PORT)
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((TCP_IP, TCP_FWD_PORT))
                self.socket.send('getResolution;&')
                data = self.recvall()
                self.calculate_ratio(data)
                break
            except Exception as e:
                print e
                print 'AltUnityServer not running on port ' + str(TCP_FWD_PORT) + ', retrying (timing out in ' + str(timeout) + ' secs)...'
                timeout -= 5
                time.sleep(5)

        if (timeout <= 0):
            raise Exception('AltUnityServer not running on port ' + str(TCP_FWD_PORT) + ', did you run ``adb forward tcp:' + str(TCP_FWD_PORT) + ' tcp:' + str(self.TCP_PORT) + '`` or ``iproxy ' + str(TCP_FWD_PORT) + ' ' + str(self.TCP_PORT) + '``?')

    def setup_port_forwarding(self, platform, port):
        if (platform == "android"):
            try:
                subprocess.Popen(['killall', 'iproxy'])
                subprocess.Popen(['adb', 'forward', 'tcp:' + str(port), 'tcp:' + str(self.TCP_PORT)])
            except:
                print 'AltUnityServer - could not use port ' + str(port)
        if (platform == "ios"):
            try:
                subprocess.Popen(['adb', 'forward', '--remove-all'])
                subprocess.Popen(['iproxy', str(port),str(self.TCP_PORT)])
            except:
                print 'AltUnityServer - could not use port ' + str(port)

    def calculate_ratio(self, data):
        if data is '':
            raise ValueError('No screen resolution data was given.')
        resolution = json.loads(data)

        self.appium_resolution = self.appium_driver.get_window_size()
        # appium resolution does not include navigation bar space, resulting in less reported height than the full screen height
        self.real_screen_size = self.appium_driver.find_element_by_xpath("//*").size;

        self.ratio = {'width': resolution['width'] / self.real_screen_size['width'],
                      'height': resolution['height'] / self.real_screen_size['height']}

        ratio_difference = ''
        if self.ratio['width'] is not 1:
            ratio_difference = 'There is a ratio difference on X of ' + str(self.ratio['width'])
        if self.ratio['height'] is not 1:
            if ratio_difference is not '':
                ratio_difference += ' and Y of ' + str(self.ratio['height'])
            else:
                ratio_difference = 'There is a ratio difference on Y of ' + str(self.ratio['height'])
        if ratio_difference is not '':
            print(ratio_difference)

    def stop(self):
        self.socket.send('closeConnection;&')
        self.socket.close()

    def recvall(self):
        data = ''
        while True:
            part = self.socket.recv(BUFFER_SIZE)
            data += part
            if '::altend' in part:
                break
        try:
            data = data.split('altstart::')[1].split('::altend')[0]
        except:
            print 'Data received from socket doesn not have correct start and end control strings'
            return ''
        return data

    def get_all_elements(self):
        alt_elements = []
        self.socket.send('findAllObjects;&')
        data = self.recvall()
        if (data != ''):
            try:
                elements = json.loads(data)
                for i in range(0, len(elements)):
                    alt_elements.append(AltElement(self, self.appium_driver, json.dumps(elements[i])))
            except Exception as e:
                print e
                print 'Received message could not be parsed: ' + data
        return alt_elements

    def find_element(self, name):
        self.socket.send('findObjectByName;' + name + ';&')
        data = self.recvall()
        if data != '' and 'error:notFound' not in data:
            alt_el = AltElement(self, self.appium_driver, data)
            # if name is 'parent/child' object should be named 'child'
            names = str.split(name, '/')
            name = names[-1]
            if alt_el.name == name and alt_el.enabled:
                print 'Element ' + name + ' found at x:' + str(alt_el.x) + ' y:' + str(alt_el.y) + ' mobileY:' + str(alt_el.mobileY)
                return alt_el
        return None

    def find_element_where_name_contains(self, name):
        self.socket.send('findObjectWhereNameContains;' + name + ';&')
        data = self.recvall()
        if (data != '' and 'error:notFound' not in data):
            alt_el = AltElement(self, self.appium_driver, data)
            if name in alt_el.name and alt_el.enabled:
                print 'Element ' + name + ' found at x:' + str(alt_el.x) + ' y:' + str(alt_el.y) + ' mobileY:' + str(alt_el.mobileY)
                return alt_el
        return None

    def find_elements(self, name):
        alt_elements = []
        self.socket.send('findObjectsByName;' + name + ';&')
        data = self.recvall()
        if (data != ''):
            try:
                elements = json.loads(data)
                for i in range(0, len(elements)):
                    alt_elements.append(AltElement(self, self.appium_driver, json.dumps(elements[i])))
            except Exception as e:
                print e
                print 'Received message could not be parsed: ' + data
        return alt_elements

    def find_elements_where_name_contains(self, name):
        alt_elements = []
        self.socket.send('findObjectsWhereNameContains;' + name + ';&')
        data = self.recvall()
        if (data != ''):
            try:
                elements = json.loads(data)
                for i in range(0, len(elements)):
                    alt_elements.append(AltElement(self, self.appium_driver, json.dumps(elements[i])))
            except Exception as e:
                print e
                print 'Received message could not be parsed: ' + data
        return alt_elements

    def get_current_scene(self):
        self.socket.send('getCurrentScene;&')
        data = self.recvall()

        if (data != ''):
            alt_el = AltElement(self, self.appium_driver, data)
            print 'Current scene is ' + alt_el.name
            return alt_el.name
        return None

    def wait_for_current_scene_to_be(self, scene_name, timeout=30, interval=1):
        t = 0
        current_scene = ''
        while (t <= timeout):
            print 'Waiting for scene to be ' + scene_name + '...'
            current_scene = self.get_current_scene()
            if current_scene != scene_name:
                time.sleep(interval)
                t += interval
            else:
                break
        assert current_scene == scene_name, 'Scene ' + scene_name + ' not loaded after ' + str(timeout) + ' seconds'
        return current_scene

    def get_element_text(self, name):
        return self.find_element(name).text

    def wait_for_element(self, name, timeout=20, interval=0.5):
        t = 0
        alt_element = None
        while (t <= timeout):
            print 'Waiting for element ' + name + '...'
            alt_element = self.find_element(name)
            if alt_element == None:
                time.sleep(interval)
                t += interval
            else:
                break
        assert alt_element is not None, 'Element ' + name + ' not found after ' + str(timeout) + ' seconds'
        return alt_element


    def wait_for_element_where_name_contains(self, name, timeout=20, interval=0.5):
        t = 0
        alt_element = None
        while (t <= timeout):
            print 'Waiting for element where name contains ' + name + '...'
            alt_element = self.find_element_where_name_contains(name)
            if alt_element == None:
                time.sleep(interval)
                t += interval
            else:
                break
        assert alt_element is not None, 'Element where name contains ' + name + ' not found after ' + str(timeout) + ' seconds'
        return alt_element

    def wait_for_element_to_not_be_present(self, name, timeout=20, interval=0.5):
        t = 0
        while (t <= timeout):
            print 'Waiting for element ' + name + ' to not be present...'
            if self.find_element(name) != None:
                time.sleep(interval)
                t += interval
            else:
                break
        assert self.find_element(name) is None, 'Element ' + name + ' still found after ' + str(timeout) + ' seconds'

    def wait_for_element_with_text(self, name, text, timeout=20, interval=0.5):
        t = 0
        alt_element = None
        while (t <= timeout):
            print 'Waiting for element ' + name + ' to have text ' + text
            alt_element = self.wait_for_element(name)
            if alt_element.text != text:
                time.sleep(interval)
                t += interval
            else:
                break
        assert alt_element.text == text, 'Element ' + name + ' should have text `' + text + '` but has `' + alt_element.text + '` after ' + str(timeout) + ' seconds'
        return alt_element

    def find_element_by_component(self, component_name):
        self.socket.send('findObjectByComponent;' + component_name + ';&')
        data = self.recvall()
        if (data != '' and 'error:notFound' not in data):
            alt_el = AltElement(self, self.appium_driver, data)
            if alt_el.name == component_name and alt_el.enabled:
                print 'Element with component ' + component_name + ' found at x:' + str(alt_el.x) + ' y:' + str(alt_el.y) + ' mobileY:' + str(alt_el.mobileY)
                return alt_el
        return None

    def find_elements_by_component(self, component_name):
        alt_elements = []
        self.socket.send('findObjectsByComponent;' + component_name + ';&')
        data = self.recvall()
        if (data != ''):
            try:
                elements = json.loads(data)
                for i in range(0, len(elements)):
                    alt_elements.append(AltElement(self, self.appium_driver, json.dumps(elements[i])))
            except Exception as e:
                print e
                print 'Received message could not be parsed: ' + data
        return alt_elements